<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./css/bootstrap.min.css" />

    <title>Hello, world!</title>
  </head>
  <body style="background: url('./img/bg.jpg');background-repeat: no-repeat;background-size: cover;">
    <div class="mt-md-5 container shadow">
        <div class="row">
            <h2 class="p-3">
                Jejak Si Septian
            </h2>
        </div>
    </div>

    <div class="container pt-4">
        <div class="row">
            <div class="col-md-8 offset-md-4">
                <h3 >Daftar Mendaki</h3>
                <hr />
              <form>
				  <div class="form-row">
					<div class="form-group col-md-6">
					  <label for="inputNIK4">NIK</label>
					  <input type="number" class="form-control" id="inputNIK4" name="nik" placeholder="NIK">
					</div>
					<div class="form-group col-md-6">
					  <label for="inputNama4">Nama</label>
					  <input type="text" class="form-control" id="inputNama4" name="nama" placeholder="Nama">
					</div>
				  </div>
				   <div class="form-row">
					<div class="form-group col-md-5">
					  <label for="inputEmail">Email</label>
					  <input type="Email" class="form-control" name="email" id="inputEmail">
					</div>
					<div class="form-group col-md-4">
					  <label for="inputHp">Nomor Hp</label>
					  <input type="number" class="form-control" name="hp" id="inputHp">
					</div>
					<div class="form-group col-md-3">
					  <label for="inputBerangkat">Berangkat</label>
					  <input type="date" class="form-control" name="berangkat" id="inputBerangkat">
					</div>
				  </div>
				    <div class="form-row">
					<div class="form-group col-md-6">
					  <label for="inputPekerjaan">Pekerjaan</label>
					  <input type="text" class="form-control" name="pekerjaan" id="inputPekerjaan">
					</div>
					<div class="form-group col-md-4">
					  <label for="inputGender">Gender</label>
					  <select id="inputGender" name="jk" class="form-control">
						<option selected>Choose...</option>
						<option value="L">Laki -Laki</option>
						<option value="P">Perempuan</option>
					  </select>
					</div>
					<div class="form-group col-md-2">
					  <label for="inputUsia">Usia</label>
					  <input type="number" class="form-control" name="usia" id="inputUsia">
					</div>
				  </div>
				  <div class="form-group">
					<label for="inputAlamat" class="text-white">Alamat</label>
					<textarea class="form-control" name="alamat" id="inputAlamat"> </textarea>
				  </div>
				  <button type="submit" class="btn btn-primary">Daftar</button>
				</form>
            </div>
        </div>
    </div>
	<div class="modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-body">
			<p>Modal body text goes here.</p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.bundle.min.js"></script>
	<script src="./js/index.js"></script>
  </body>
</html>