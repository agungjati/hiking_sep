-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2018 at 04:36 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hiking_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_pendaki`
--

CREATE TABLE `data_pendaki` (
  `NIK` varchar(16) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `usia` int(11) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `hp` varchar(12) NOT NULL,
  `email` text NOT NULL,
  `tgl_berangkat` date NOT NULL,
  `tgl_kembali` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `registrasi`
--

CREATE TABLE `registrasi` (
  `NIK` varchar(16) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `usia` int(11) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `hp` varchar(12) NOT NULL,
  `email` text NOT NULL,
  `tgl_berangkat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registrasi`
--

INSERT INTO `registrasi` (`NIK`, `nama`, `jk`, `alamat`, `usia`, `pekerjaan`, `hp`, `email`, `tgl_berangkat`) VALUES
('', '', '', ' ', 0, '', '', '', '2018-12-12'),
('10', 'Agung Jati', 'L', 'Batam', 19, 'Backend C#', '081534656616', 'pendaki@gmail.com', '2018-12-10'),
('101', 'Darul', 'L', 'Pasar Wesi', 19, 'Shooter', '081512231234', 'pendaki@gmail.com', '2018-12-10'),
('1011', 'Ikhsan', 'L', 'Sunggorungi', 19, 'Kuliah', '081512231234', 'pendaki@gmail.com', '2018-12-10'),
('10111', 'Ian', 'L', 'Bekonang', 19, 'Kuliah', '081512233214', 'pendaki@gmail.com', '2018-12-10'),
('1212', 'Septian', 'L', ' Boyolali', 20, 'Kuliah', '08080080', 'pendaki@gmail.com', '2018-11-29'),
('12121', 'Anisa', 'L', 'Bandung', 20, 'Kuliah', '08080080', 'pendaki@gmail.com', '2018-11-29'),
('121211', 'asads', 'L', 'Bandung', 20, 'Kuliah', '08080080', 'pendaki@gmail.com', '2018-11-29'),
('1212111', 'asads', 'L', 'Bandung', 20, 'Kuliah', '08080080', 'pendaki@gmail.com', '2018-11-29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `nama`) VALUES
(1, 'admin', '8cb2237d0679ca88db6464eac60da9', 'linda'),
(3, 'anisa', '21232f297a57a5a743894a0e4a801fc3', 'anisa'),
(4, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_pendaki`
--
ALTER TABLE `data_pendaki`
  ADD KEY `data_pendaki` (`NIK`);

--
-- Indexes for table `registrasi`
--
ALTER TABLE `registrasi`
  ADD PRIMARY KEY (`NIK`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_pendaki`
--
ALTER TABLE `data_pendaki`
  ADD CONSTRAINT `data_pendaki` FOREIGN KEY (`NIK`) REFERENCES `registrasi` (`NIK`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
