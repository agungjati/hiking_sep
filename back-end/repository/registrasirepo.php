<?php
class RegistrasiRepo {

	private $conn;

	public function __construct($conn) {
		$this->conn = $conn;
	}

	public function Save(array $p = array()) 
	{
		$query = "INSERT INTO `registrasi` 
		(`NIK`, `nama`, `jk`, `alamat`, `usia`, `pekerjaan`, `hp`, `email`, `tgl_berangkat`) 
		VALUES 
		('".$p['nik']."', '".$p['nama']."', '".$p['jk']."', '".$p['alamat']."', '".$p['usia']."', 
		'".$p['pekerjaan']."', '".$p['hp']."', '".$p['email']."', '".$p['berangkat']."')";

		$result =  $this->conn->prepare($query);

		return $result->execute();

	}

	public function Get(int $id = 0) {
		if($id){
			$query = "SELECT * FROM `registrasi` `NIK`='".$id."'";
			$response = array();
			if ($result = $this->conn->query($query)) {
				$row = $result->fetch_assoc();
				$response = array (
					"NIK" => $row["NIK"],
					"nama" => $row["nama"],
					"jk" => $row["jk"],
					"alamat" => $row["alamat"],
					"usia" => $row["usia"],
					"pekerjaan" => $row["pekerjaan"],
					"hp" => $row["hp"],
					"email" => $row["email"],
					"tgl_berangkat" => $row["tgl_berangkat"]
				);

				$result->free();
			}
			
			return $response;
		}else {
			$query = "SELECT * FROM `registrasi` ";
			$response = array();
			if ($result = $this->conn->query($query)) {
			
				while ($row = $result->fetch_assoc()) {
					$arr = array (
					"NIK" => $row["NIK"],
					"nama" => $row["nama"],
					"jk" => $row["jk"],
					"alamat" => $row["alamat"],
					"usia" => $row["usia"],
					"pekerjaan" => $row["pekerjaan"],
					"hp" => $row["hp"],
					"email" => $row["email"],
					"tgl_berangkat" => $row["tgl_berangkat"]
					);

					array_push($response , $arr);
				}

				$result->free();
			}
			
			return $response;

		}
	}

	public function Delete(int $nik)
	{
		$query = "DELETE FROM `registrasi` WHERE `registrasi`.`NIK` = '".$nik."'";
		$result =  $this->conn->prepare($query);
		return $result->execute();
	}

}
?>