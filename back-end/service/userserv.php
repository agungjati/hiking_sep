<?php
include("../repository/connection.php");
include("../repository/userrepo.php");

$conn = new Connection();
$service = new UserRepo($conn->getConnect());

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$data = array(
		"username" => $_POST["username"],
		"password" =>  $_POST["password"]
	);
	$res = $service->Login($data);
	if(count($res) != 0) {
		$_SESSION["nama"] = $res["nama"];
		$_SESSION["username"] = $res["username"];
		echo(json_encode($res));
	}
	else {
		header("HTTP/1.1 400 Bad Request");
		echo(json_encode(array( "Message" => "Error" )));
	}
}
else if($_SERVER['REQUEST_METHOD'] == "GET")
{
	
}
?>