<?php
include("../repository/connection.php");
include("../repository/registrasirepo.php");

$conn = new Connection();
$service = new RegistrasiRepo($conn->getConnect());

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$data = array(
		"nik" =>  $_POST["nik"],
		"nama" => $_POST["nama"],
		"alamat" => $_POST["alamat"],
		"berangkat" =>  $_POST["berangkat"],
		"email" => $_POST["email"],
		"hp" => $_POST["hp"],
		"jk" =>	$_POST["jk"],
		"pekerjaan" => $_POST["pekerjaan"],
		"usia" => $_POST["usia"]
	);

	if($service->Save($data)){
		echo(json_encode(array( "Message" => "Success" )));
	}else {
		header("HTTP/1.1 400 Bad Request");
		echo(json_encode(array( "Message" => "Error")));
	}
}
else if($_SERVER['REQUEST_METHOD'] == "GET")
{
	 echo(json_encode($service->Get()));
} 
else if($_SERVER['REQUEST_METHOD'] == "DELETE") {
	parse_str(file_get_contents("php://input") , $_DELETE);
	echo(json_encode($service->Delete($_DELETE["nik"])));
}
?>