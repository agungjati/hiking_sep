(function () {
    $("form").on("submit", (e) => {
        e.preventDefault()
        let serialize = $(e.currentTarget).serializeArray(),
            data = {}

        serialize.forEach(e => {
            data[e.name] = e.value
        })

        $.ajax({
            url: "back-end/service/registrasiserv.php",
            type: "POST",
            data: data,
            success(data) {
                try {
                    $(".modal .modal-body p").html("Berhasil Disimpan");
                    $(".modal").modal()
                    console.log(JSON.parse(data))
                } catch (e) {
                    $(".modal .modal-body p").html(data);
                    $(".modal").modal()
                    console.log((data))
                }
            },
            error(err) {
                let msg;
                try {
                    msg = JSON.parse(err.responseText);
                    $(".modal .modal-body p").html(msg.Message);
                } catch (e) {
                    $(".modal .modal-body p").html(err.statusText);
                }
                
                $(".modal").modal()
            }
        })
    })
})()