(function () {
    $.ajax({
        url: "back-end/service/registrasiserv.php",
        type: "GET",
        success(data) {
            let pendaftar = JSON.parse(data)
            $(".table tbody").html("")
            pendaftar.forEach(setElemTr)
        },
        error(err) {
            console.log(err.statusText)
        }
    })

    function setElemTr(p , i) {
        $(".table tbody").append(`
        <tr style="cursor: pointer" id="${ p.NIK }">
            <td>${ ++i }</td>
            <td>${ p.NIK }</td>
            <td Nama>${ p.nama }</td>
            <td>${ p.jk }</td>
            <td>${ p.usia}</td>
            <td>${ p.pekerjaan }</td>
            <td>${ p.hp}</td>
            <td>${ p.email }</td>
            <td>${ p.tgl_berangkat }</td>
        </tr>
         `)
        
        $("tr").on("click", function (e) {
            $(".modal [Nama]").html($(e.currentTarget).find("td[Nama]").html())
            $(".modal input[name='nik']").val($(e.currentTarget).attr("id"))
            $(".modal").modal()
        })
    }

    $("form").on("click", (e) => {
        e.preventDefault()
        let serialize = $(e.currentTarget).serializeArray(),
            data = {}

        serialize.forEach(e => {
            data[e.name] = e.value
        })

        if ($(e.target).html() == "Hapus") {
            $.ajax({
                url: "back-end/service/registrasiserv.php",
                data : data,
                type: "DELETE",
                success(res) {
                    if (JSON.parse(res)) {
                        $(".table tr[id='" + data["nik"] + "']").remove()
                        $(".modal").modal("hide")
                    }
                    
                },
                error(err) {
                    console.log(err.statusText)
                }
            })     
        }
    })
})()