(function () {
    $("form").on("submit", (e) => {
        e.preventDefault()
        let serialize = $(e.currentTarget).serializeArray(),
            data = {}

        serialize.map(e => {
            data[e.name] = e.value
        })

        console.log(data)
        $.ajax({
            url: "back-end/service/userserv.php",
            type: "POST",
            data: data,
            success(data) {
                try {
                    const res = JSON.parse(data)
                    if (res["nama"]) {
                        window.location = "dashboard.html"
                    } else {
                        $(".modal .modal-body p").html("Gagal login");
                        $(".modal").modal()
                    }
                    console.log(res)

                } catch (e) {
                    $(".modal .modal-body p").html(data);
                    $(".modal").modal()
                    console.log(data)
                }
            },
            error(err) {
                let msg;
                try {
                    msg = JSON.parse(err.responseText);
                    $(".modal .modal-body p").html(msg.Message);
                } catch (e) {
                    $(".modal .modal-body p").html(err.statusText);
                }

                $(".modal").modal()
            }
        })
    })
})()